#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# importar
import re
import urllib2
    
USUARIO = 'LaCuriosidadDelGato'                                     # usuario de la cuenta
INSTANCIA = 'https://mastodon.social'                               # instancia en la que está
URL_SIGUIENDO = INSTANCIA+'/users/'+USUARIO+'/following'
URL_SEGUIDORES = INSTANCIA+'/users/'+USUARIO+'/followers'

# pide y devuelve el html de la página pedida por get
def getPagina(url,cabeceras = False):
    try:
        f = urllib2.urlopen(url)
        temp = f.read()
        urlReal = f.geturl()
        f.close()
        return [temp.replace('\n',' '),urlReal]
    except HTTPError, e:
        print("Ocurrio un error")
        print(e.code)
    except URLError, e:
        print("Ocurrio un error")
        print(e.reason)

# devuelve el texto que esté entre los patrones de texto que escribas
def extraerCadena(patronIni,patronFin,contenido):
    patron = patronIni + "(.*?)" + patronFin
    return re.findall(patron, contenido)

# devuelve una lista de los usuarios seguidos o seguidores si le pasas 'following' o 'followers'
def listaSiguendoSeguidores(var):
    print('Buscando '+var+' en página:')
    siguiendo = []
    paginaSiguiente = 1
    paginaAhora = 0
    while(paginaSiguiente>paginaAhora):
        paginaAhora = paginaSiguiente
        print(paginaAhora)
        if paginaAhora==1:
            html = getPagina(INSTANCIA+'/users/'+USUARIO+'/'+var)
        else:
            html = getPagina(INSTANCIA+'/users/'+USUARIO+'/'+var+'?page='+str(paginaSiguiente))
        siguiendo = siguiendo + extraerCadena('bdi> <span> ',' <i',html[0])
        pagina = extraerCadena(var+'\?page=','">',html[0])
        paginaSiguiente = int(pagina[-1])
    
    return siguiendo 

def listaSiguiendo():
    return listaSiguendoSeguidores('following')

def listaSeguidores():
    return listaSiguendoSeguidores('followers')



if __name__ == "__main__":
    siguiendo = listaSiguiendo()
    seguidores = listaSeguidores()
    noSigue = []
    noSigo = []

    for seguido in siguiendo:
        if seguido in seguidores:
            pass
        else:
            noSigue.append(seguido)
            
    for seguidor in seguidores:
        if seguidor in siguiendo:
            pass
        else:
            noSigo.append(seguidor)
        
    print('Encontrados '+str(len(seguidores))+' seguidores y '+str(len(siguiendo))+' seguidos.')
    print('De los cuales '+str(len(noSigue))+' no me siguen:')
    print('   '+'\n   '.join(noSigue))
    print('')
    print('Y yo no sigo a '+str(len(noSigo))+':')
    print('   '+'\n   '.join(noSigo))
