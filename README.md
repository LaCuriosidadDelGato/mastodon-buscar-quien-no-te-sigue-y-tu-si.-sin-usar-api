# MASTODON - Buscar quien no te sigue y tu si y al revés. Sin usar API

La mejor manera de hacerlo es con la [API propia de mastodon](https://docs.joinmastodon.org/client/libraries/) pero me apetecía realizar un ejemplo de como extraer información de páginas html

## Utilización

Modificar dos datos de usuario e instancia por el usuario que se quiera hacer. Para que funcione la cuenta tiene que tener pública sus seguidos/seguidores:

```
USUARIO = 'LaCuriosidadDelGato'
INSTANCIA = 'https://mastodon.social'
```
